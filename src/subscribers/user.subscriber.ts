import { ApolloError } from 'apollo-server';
import { validate } from 'class-validator';
import { EntitySubscriberInterface, EventSubscriber, InsertEvent } from 'typeorm';

import User from '~/entities/user.entity';
import BadRequestException from '~/exceptions/bad-request.exception';

@EventSubscriber()
export class EverythingSubscriber implements EntitySubscriberInterface<User> {
  /**
   * Called before entity insertion.
   */
  async beforeInsert(event: InsertEvent<User>): Promise<void> {
    const errors = await validate(event.entity);
    if (errors.length > 0) {
      throw new ApolloError(
        `Error de validación`,
        '400',
        errors.map((item) => item.constraints)
      );
    }
    const hasOne = await event.manager.findOne(User, { email: event.entity.email });
    if (hasOne) {
      throw new BadRequestException('El usuario ya existe');
    }
    console.log(`BEFORE ENTITY INSERTED: `, hasOne);
  }
}
