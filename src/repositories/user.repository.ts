import { EntityRepository, MongoRepository } from 'typeorm';
import { ObjectID } from 'mongodb';


import User from '~/entities/user.entity';

@EntityRepository(User)
export default class UserRepository extends MongoRepository<User> {
  findById(id: ObjectID): Promise<User | undefined> {
    return this.findOne({
      where:{
      _id: new ObjectID(id)
    }});
  }

  findByEmail(email: string): Promise<User | undefined> {
    return this.findOne({ email });
  }
}
