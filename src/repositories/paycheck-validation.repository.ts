import { EntityRepository, Repository } from 'typeorm';

import PaycheckValidation from '~/entities/paycheck/paycheck_validation.entity';

@EntityRepository(PaycheckValidation)
export default class PaycheckValidationRepository extends Repository<PaycheckValidation> {


}
