import crypto from 'crypto';

import Config from '~/config';

export default class CryptrHelper {
  static encript(str: string): string {
    const paswordComplete = `${Config.CRYPTO_SUFFIX}${str}${Config.CRYPTO_PREFIX}`;
    const hash = crypto.createHash('sha256').update(paswordComplete).digest('base64');
    return hash;
  }

  static compare(myPlaintextPassword: string, hash: string): boolean {
    const encript = CryptrHelper.encript(myPlaintextPassword);
    return encript === hash;
  }
}
