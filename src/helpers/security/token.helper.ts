import * as jsonwebtoken from 'jsonwebtoken';
import { ObjectID } from 'mongodb';

import Config from '~/config';

export interface TokenInterface {
  id: ObjectID;
  email: string;
}

export const encode = (payload: TokenInterface): string => {
  return jsonwebtoken.sign(JSON.stringify(payload), Config.SECRET);
};

export const decode = (token: string): TokenInterface => {
  return jsonwebtoken.verify(token, Config.SECRET) as TokenInterface;
};



export const shared_encode = (payload: unknown): string => {
  console.log({s:Config.API_SHARED_SECRET});
  return jsonwebtoken.sign(JSON.stringify(payload), Config.API_SHARED_SECRET);
};

export const shared_decode = (token: string): unknown => {
  return jsonwebtoken.verify(token, Config.API_SHARED_SECRET) as unknown;
};