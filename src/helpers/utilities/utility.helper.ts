

export default class UtilityHelper {
    /**
     * Columnize
     * @param input 
     * @param cols 
     */
    columnize(input: Array<any>, cols: number) {
        const chunks:any = [];
        let i = 0;
        const n = input.length;

        while (i < n) {
          chunks.push(input.slice(i, i += cols));
        }
        return chunks;
      }

}
