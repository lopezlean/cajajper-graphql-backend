import Log4JSHelper from './log4js.helper';
import { Container } from 'typedi';

const loggerHelper = Container.get(Log4JSHelper);
const logger = loggerHelper.logger.getLogger('INSPEKT');

export default logger;
