import log4js,{Configuration} from 'log4js';

const config:Configuration = {
  appenders: {
    out: { type: 'stdout' },
    app: { type: 'file', filename: 'graphql.log' }
  },
  categories: {
    default: { appenders: [ 'out', 'app' ], level: "DEBUG"}
  }
}

export default class Log4JSHelper {

    public logger = log4js.configure(config);
}
