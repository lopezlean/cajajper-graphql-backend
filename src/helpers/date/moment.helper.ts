const moment = require('moment-timezone');
moment.tz.setDefault('Europe/London');

export default moment;
