// Moment
import Moment from 'moment';

import moment from 'moment-timezone';

export default class DateHelper {
  transformDate(date: Date, format: string, locale = 'en'): string {
    // Moment.locale(locale);
    return Moment(date).format(format);
  }

  timeToString(time) {
    const date = new Date(time);
    return date.toISOString().split('T')[0];
  }
  // Formate date
  formatDateAsDay(date) {
    let d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    const dateString = [year, month, day].join('-');
    const timestamp = d.getTime();
    return { year, month, day, dateString, timestamp };
  }

  fixTimeZone(date) {
    const year = moment(date).tz('GMT+0').format('YYYY');
    const month = moment(date).tz('GMT+0').format('MM');
    const day = moment(date).tz('GMT+0').format('DD');
    const hour = moment(date).tz('GMT+0').format('HH');
    const minutes = moment(date).tz('GMT+0').format('mm');
    return new Date(
      parseInt(year),
      parseInt(month) - 1,
      parseInt(day),
      parseInt(hour),
      parseInt(minutes)
    );
  }
}
