import { Field, ID, Int, ObjectType } from 'type-graphql';
import { Column, Entity,  PrimaryGeneratedColumn } from 'typeorm';

@ObjectType()
@Entity('solicitantes')
export default class Applicant {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field(() => Int)
  @Column({ name: 'expediente_id' })
  document_id: number;

  @Field(() => Int)
  @Column({ name: 'tipo_documento_id' })
  national_identification_type_id: number;

  @Field(() => Int)
  @Column({ name: 'estados_civil_id' })
  civil_status_id: number;

  @Field(() => Int)
  @Column({ name: 'orden' })
  order: number;

  @Field(() => Int)
  @Column({ name: 'documento' })
  national_identification_number: number;

  @Field()
  @Column({ name: 'cuil' })
  cuil: string;

  @Field()
  @Column({ name: 'nombre' })
  first_name: string;

  @Field()
  @Column({ name: 'apellido' })
  last_name: string;

  @Field()
  @Column({ name: 'email' })
  email: string;
}
