import { Field, ID, ObjectType } from 'type-graphql';
import { Column, Entity,  PrimaryGeneratedColumn } from 'typeorm';

@Entity('tipo_solicitudes')
@ObjectType()
export default class RequestType {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column({ name: 'nombre' })
  name: string;
}
