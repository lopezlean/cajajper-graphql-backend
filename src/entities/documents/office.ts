import { Field, ID, ObjectType } from 'type-graphql';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('areas')
@ObjectType()
export default class Office {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column({ name: 'nombre' })
  name: string;

  @Field()
  @Column({ name: 'nombre_web' })
  web_name: string;
}
