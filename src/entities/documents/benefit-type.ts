import { Field, ID, ObjectType } from 'type-graphql';
import { Column, Entity,  PrimaryGeneratedColumn } from 'typeorm';

@Entity('tipo_beneficios')
@ObjectType()
export default class BenefitType {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column({ name: 'nombre' })
  name: string;
}
