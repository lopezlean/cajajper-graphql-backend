import { Column, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Field, ID, Int, ObjectType } from 'type-graphql';

import Applicant from './applicant';
import BenefitType from './benefit-type';
import RequestType from './request-type';
import Office from './office';
import Movement from './movement';


@Entity('expedientes')
@ObjectType()
export default class Document {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id!: number;



  @Field(() => Boolean)
  @Column({ name: 'pre_carga' })
  is_draft: boolean; // is true the document is a draft

  @Field({ nullable: true })
  @Column({ name: 'solicitante' })
  applicant: string;

  @Field({ nullable: true })
  @Column({ name: 'asunto' })
  subject: string;

  @Field({ nullable: true })
  @Column({ name: 'observaciones' })
  comments: string;

  @Field(() => Int, { nullable: true })
  @Column({ name: 'numero_fojas' })
  number_of_pages: number;

  @Field(() => Int, { nullable: true })
  @Column({ name: 'parent_id' })
  parent_id: number;

  @Field(() => Int, { nullable: true })
  @Column({ name: 'area_id' })
  office_id: number;

  @Field(() => Int)
  @Column({ name: 'tipo_solicitud_id' })
  request_type_id: number;


  @Field(() => Int)
  @Column({ name: 'tipo_beneficio_id' })
  benefit_type_id: number;

  @Field(() => [Applicant])
  @OneToMany(() => Applicant, aplicant => aplicant.document_id)
  applicants: Applicant[]

  @Field(() => BenefitType)
  @OneToOne(() => BenefitType)
  @JoinColumn({ name: 'tipo_beneficio_id' })
  benefit_type: BenefitType

  @Field(() => RequestType)
  @OneToOne(() => RequestType)
  @JoinColumn({ name: 'tipo_solicitud_id' })
  request_type: RequestType

  @Field(() => Office, {nullable: true})
  @OneToOne(() => Office)
  @JoinColumn({ name: 'area_id' })
  office: Office | null

  @Field(() => [Movement], { nullable: true })
  @OneToMany(() => Movement, movement => movement.document_id)
  last_movements: Movement[] | null

}

