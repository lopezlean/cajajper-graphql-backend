import { Field, ID, Int, ObjectType } from 'type-graphql';
import { Column, Entity,  PrimaryGeneratedColumn } from 'typeorm';


import Office from './office';

@ObjectType()
@Entity('pase_detalles')
export default class Movement {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;
  
  @Field(()=> Int)
  @Column({ name: 'numero_fojas' })
  number_of_pages: number;

  @Field(()=> Int)
  @Column({ name: 'expediente_id' })
  document_id: number;

  @Field(()=> Int)
  @Column({ name: 'area_id' })
  office_id: number;

  @Field(()=> Int)
  @Column({ name: 'estado' })
  status: number;

  @Field()
  @Column({ name: 'created' })
  created: Date;

  @Field()
  @Column({ name: 'modified' })
  modified: Date;

  @Field(()=>Office)
  
  origin_office: Office;

  @Field(()=>Office)
  
  office: Office;
}
