import { Field, ID, Int, ObjectType } from 'type-graphql';
import { Entity, ObjectIdColumn, Column } from 'typeorm';
import PaycheckUser from './paycheck/paycheck-user.entity';

@Entity()
@ObjectType()
export default class PaycheckUserList {
  @Field(() => ID, { nullable: true })
  @ObjectIdColumn({ name: '_id' })
  id!: string | null;
  @Column()
  @Field((type) => [Int], { nullable: true })
  ids: [number];


  @Field((type) => [PaycheckUser], { nullable: true })
  users: PaycheckUser[];
}
