import { Field, ID, ObjectType } from 'type-graphql';
import { ObjectID } from 'mongodb';

@ObjectType()
export default class Auth {
  @Field(() => ID)
  id: ObjectID;
  @Field()
  email: string;
  @Field()
  token: string;
}
