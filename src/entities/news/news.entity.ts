import { Field, ID, ObjectType } from 'type-graphql';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('novedades')
@ObjectType()
export default class News {
    @Field(() => ID)
    @PrimaryGeneratedColumn()
    id: number;

    @Field()
    @Column({ name: 'titulo' })
    title: string;

    @Field()
    @Column({ name: 'texto' })
    body: string;

    @Field()
    url: string;

    @Field()
    @Column({ name: 'fecha' })
    date: string;

    @Field({ nullable: true })
    @Column({ name: 'imgsrc' })
    image: string;

    @Field()
    @Column({ name: 'estado' })
    status: boolean;

}

