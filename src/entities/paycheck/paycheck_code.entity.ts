import { Field, ID, Int, ObjectType } from 'type-graphql';
import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('detallearchivo_codigos')
@ObjectType()
export default class PaycheckCode {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field(() => Int)
  @Column({ name: 'idArchivo' })
  file_id: number;

  @Field(() => Int)
  @Column({ name: 'mesLiquidacion' })
  month: number;

  @Field(() => Int)
  @Column({ name: 'anioLiquidacion' })
  year: number;

  @Field(() => String)
  @Column({ name: 'tipoLiquidacion' })
  settlement_type: string;

  @Field(() => Int)
  @Column({ name: 'numeroCodigo' })
  code_number: number;

  @Field(() => String)
  @Column({ name: 'descripcionCodigo' })
  description: string;

  @Field(() => String)
  @Column({ name: 'tipoCodigo' })
  type: string;
}
