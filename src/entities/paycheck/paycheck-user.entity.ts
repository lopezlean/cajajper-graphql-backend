import { Field, ID, Int, ObjectType } from 'type-graphql';
import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('miembros')
@ObjectType()
export default class PaycheckUser {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column()
  email: string;

  @Column()
  password: string;

  @Field(() => Int)
  @Column({ name: 'estado' })
  status: number;


  @Column()
  salt: string;


  @Column({ name: 'confirmado' })
  confirmed: number;

  @Field()
  @Column({ name: 'apellidoNombres' })
  full_name: string;
  @Field(() => Int)
  @Column({ name: 'numeroDocumento' })
  national_identification_number: number;

  @Field(() => Int)
  @Column({ name: 'numeroCuenta' })
  account_number: number;

  @Field(() => String)
  @Column({ name: 'numeroCuentaCompleto' })
  full_account_number: string;
}
