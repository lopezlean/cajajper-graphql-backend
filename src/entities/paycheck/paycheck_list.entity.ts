import { Field, Int, ObjectType } from 'type-graphql';

import PaycheckListItem from './paycheck_list_item.entity';

@ObjectType()
export default class PaycheckList {
  @Field(() => Int)
  year: number;
  @Field(() => Int)
  month: number;
  @Field(() => [PaycheckListItem], { nullable: true })
  paycheck?: [PaycheckListItem | undefined];
}
