import { Field, Float, ID, Int, ObjectType } from 'type-graphql';
import { Entity, PrimaryGeneratedColumn, Column, JoinColumn, OneToOne } from 'typeorm';
import PaycheckAnnex from './paycheck_annex.entity';
import PaycheckCode from './paycheck_code.entity';

@Entity('detallearchivo_recibos')
@ObjectType()
export default class Paycheck {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field(() => Int)
  @Column({ name: 'idArchivo' })
  file_id: number;

  @Field(() => Int)
  @Column({ name: 'idTipoArchivo' })
  file_type_id: number;

  @Field(() => Int)
  @Column({ name: 'mesLiquidacion' })
  month: number;

  @Field(() => Int)
  @Column({ name: 'anioLiquidacion' })
  year: number;

  @Field(() => String)
  @Column({ name: 'tipoLiquidacion' })
  type: string;

  @Field(() => Int)
  @Column({ name: 'idAnexo' })
  annex_id: number;

  @Field(() => Int)
  @Column({ name: 'numeroDocumento' })
  national_identification_number: number;

  @Field(() => String)
  @Column({ name: 'legajo' })
  file_number: string;

  @Field(() => Int)
  @Column({ name: 'idLocalidad' })
  location_id: number;

  @Field(() => String)
  @Column({ name: 'apellidoNombres' })
  full_name: string;


  @Field(() => Int)
  @Column({ name: 'numeroCodigo' })
  code_number: number;

  @Field(() => Float)
  @Column({ name: 'montoCodigo' })
  amount: number;

  @Field(() => Int)
  @Column({ name: 'numeroCuenta' })
  account_number: number;

  @Field(() => Int)
  @Column({ name: 'idSucursal' })
  branch_office_id: number;

  @Field(() => String)
  @Column({ name: 'descripcionCuenta' })
  full_account_number: string;

  @Field(() => String)
  @Column({ name: 'periodoLiquidacion' })
  period: string;

  @Field(() => PaycheckCode)
  @OneToOne(() => PaycheckCode)
  @JoinColumn({ name: 'numeroCodigo' })
  paycheck_code: PaycheckCode;

  @Field(() => PaycheckAnnex)
  @OneToOne(() => PaycheckAnnex)
  @JoinColumn({ name: 'idAnexo' })
  paycheck_annex: PaycheckAnnex;

}