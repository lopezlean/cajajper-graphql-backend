import { Field, ID, Int, ObjectType } from 'type-graphql';
import { Entity, PrimaryGeneratedColumn, Column} from 'typeorm';


@Entity('detallearchivo_recibos')
@ObjectType()
export default class PaycheckListItem {
  @Field(() => ID, { nullable: true })
  @PrimaryGeneratedColumn()
  id: number | null;

  @Field(() => Int)
  @Column({ name: 'idArchivo' })
  file_id: number;

  @Field(() => Int)
  @Column({ name: 'idTipoArchivo' })
  file_type_id: number;

  @Field(() => Int)
  @Column({ name: 'mesLiquidacion' })
  month: number;

  @Field(() => Int)
  @Column({ name: 'anioLiquidacion' })
  year: number;

  @Field(() => String)
  @Column({ name: 'tipoLiquidacion' })
  type: string;

  @Field(() => Int)
  @Column({ name: 'idAnexo' })
  annex_id: number;

  @Field(() => Int)
  @Column({ name: 'numeroDocumento' })
  national_identification_number: number;

  @Field(() => String)
  @Column({ name: 'apellidoNombres' })
  full_name: string;

  @Field(() => Int)
  @Column({ name: 'numeroCuenta' })
  account_number: number;

  @Field(() => Int)
  @Column({ name: 'idSucursal' })
  branch_office_id: number;

  @Field(() => String)
  @Column({ name: 'descripcionCuenta' })
  full_account_number: string;

  @Field(() => String)
  @Column({ name: 'periodoLiquidacion' })
  period: string;


}