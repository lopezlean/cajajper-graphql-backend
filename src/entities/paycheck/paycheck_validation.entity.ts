import { Field, ID, Int, ObjectType } from 'type-graphql';
import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('validacionrecibo')
@ObjectType()
export default class PaycheckValidation {
    @Field(() => ID)
    @PrimaryGeneratedColumn()
    id: number;

    @Field(() => Date)
    @Column({ name: 'fechaCreacion' })
    created: Date;



    @Field(() => String)
    @Column({ name: 'guid' })
    guid: string;

    @Field(() => String)
    @Column({ name: 'idMiembro' })
    paycheck_user_id: string;


    @Field(() => String)
    @Column({ name: 'numeroDocumento' })
    national_identification_number: string;


    @Field(() => String)
    @Column({ name: 'numeroCuentaCompleto' })
    full_account_number: string;


    @Field(() => String)
    @Column({ name: 'mesLiquidacion' })
    month: string;


    @Field(() => String)
    @Column({ name: 'anioLiquidacion' })
    year: string;

    @Field(() => String)
    @Column({ name: 'idTipoArchivo' })
    file_type_id: string;


    @Field(() => String)
    @Column({ name: 'idAnexo' })
    annex_id: string;


    @Field(() => String)
    @Column({ name: 'tipoLiquidacion' })
    type: string;


}
