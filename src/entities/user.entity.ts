import { IsEmail } from 'class-validator';
import { ObjectID } from 'mongodb';
import { Field, ID, ObjectType } from 'type-graphql';
import { Entity, ObjectIdColumn, Column, ObjectID as ObjectIDType } from 'typeorm';
import favoriteDocumentListEntity from './dist/favorite-document-list.entity';
import FavoriteDocumentList from './favorite-document-list.entity';

import PaycheckUserList from './paycheck-user-list.entity';

@Entity({ name: 'users' })
@ObjectType({ description: 'The recipe model' })
export default class User {
  @Field(() => ID)
  @ObjectIdColumn({ name: '_id' })
  id: ObjectIDType;
  @Field()
  @Column()
  @IsEmail({}, { message: 'Debes ingresar un email correcto' })
  email: string;
  @Field()
  @Column()
  password: string;
  @Field()
  @Column()
  active: boolean;

  @Field()
  @Column()
  activation_code: string;

  @Field({ nullable: true })
  @Column()
  first_name: string;

  @Field({ nullable: true })
  @Column()
  last_name: string;

  @Field(() => PaycheckUserList, { nullable: true })
  @Column()
  paycheck_users: PaycheckUserList | null;

  @Field(() => FavoriteDocumentList, { nullable: true })
  @Column()
  favorite_documents: FavoriteDocumentList | null;
}
