"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var type_graphql_1 = require("type-graphql");
var typeorm_1 = require("typeorm");
var document_entity_1 = require("./documents/document.entity");
var FavoriteDocumentList = /** @class */ (function () {
    function FavoriteDocumentList() {
    }
    __decorate([
        type_graphql_1.Field(function () { return type_graphql_1.ID; }, { nullable: true }),
        typeorm_1.ObjectIdColumn({ name: '_id' })
    ], FavoriteDocumentList.prototype, "id");
    __decorate([
        typeorm_1.Column(),
        type_graphql_1.Field(function (type) { return [type_graphql_1.Int]; }, { nullable: true })
    ], FavoriteDocumentList.prototype, "ids");
    __decorate([
        type_graphql_1.Field(function (type) { return [document_entity_1["default"]]; }, { nullable: true })
    ], FavoriteDocumentList.prototype, "documents");
    FavoriteDocumentList = __decorate([
        typeorm_1.Entity(),
        type_graphql_1.ObjectType()
    ], FavoriteDocumentList);
    return FavoriteDocumentList;
}());
exports["default"] = FavoriteDocumentList;
