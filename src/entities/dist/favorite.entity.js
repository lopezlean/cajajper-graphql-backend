"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var type_graphql_1 = require("type-graphql");
var typeorm_1 = require("typeorm");
var Favorite = /** @class */ (function () {
    function Favorite() {
    }
    __decorate([
        type_graphql_1.Field(function () { return type_graphql_1.ID; }),
        typeorm_1.ObjectIdColumn({ name: '_id' })
    ], Favorite.prototype, "id");
    __decorate([
        type_graphql_1.Field(function () { return typeorm_1.ObjectID; }),
        typeorm_1.Column()
    ], Favorite.prototype, "user_id");
    __decorate([
        type_graphql_1.Field(function () { return type_graphql_1.Int; }),
        typeorm_1.Column()
    ], Favorite.prototype, "document_id");
    Favorite = __decorate([
        typeorm_1.Entity({ name: 'favorites' }),
        type_graphql_1.ObjectType({ description: 'Favorites model' })
    ], Favorite);
    return Favorite;
}());
exports["default"] = Favorite;
