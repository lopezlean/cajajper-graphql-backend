"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var class_validator_1 = require("class-validator");
var type_graphql_1 = require("type-graphql");
var typeorm_1 = require("typeorm");
var favorite_document_list_entity_1 = require("./favorite-document-list.entity");
var paycheck_user_list_entity_1 = require("./paycheck-user-list.entity");
var User = /** @class */ (function () {
    function User() {
    }
    __decorate([
        type_graphql_1.Field(function () { return type_graphql_1.ID; }),
        typeorm_1.ObjectIdColumn({ name: '_id' })
    ], User.prototype, "id");
    __decorate([
        type_graphql_1.Field(),
        typeorm_1.Column(),
        class_validator_1.IsEmail({}, { message: 'Debes ingresar un email correcto' })
    ], User.prototype, "email");
    __decorate([
        type_graphql_1.Field(),
        typeorm_1.Column()
    ], User.prototype, "password");
    __decorate([
        type_graphql_1.Field(),
        typeorm_1.Column()
    ], User.prototype, "active");
    __decorate([
        type_graphql_1.Field(),
        typeorm_1.Column()
    ], User.prototype, "activation_code");
    __decorate([
        type_graphql_1.Field({ nullable: true }),
        typeorm_1.Column()
    ], User.prototype, "first_name");
    __decorate([
        type_graphql_1.Field({ nullable: true }),
        typeorm_1.Column()
    ], User.prototype, "last_name");
    __decorate([
        type_graphql_1.Field(function () { return paycheck_user_list_entity_1["default"]; }, { nullable: true }),
        typeorm_1.Column()
    ], User.prototype, "paycheck_users");
    __decorate([
        type_graphql_1.Field(function () { return favorite_document_list_entity_1["default"]; }, { nullable: true }),
        typeorm_1.Column()
    ], User.prototype, "favorite_documents");
    User = __decorate([
        typeorm_1.Entity({ name: 'users' }),
        type_graphql_1.ObjectType({ description: 'The recipe model' })
    ], User);
    return User;
}());
exports["default"] = User;
