import { config } from 'dotenv';
import { resolve } from 'path';

config({ path: resolve(__dirname, '../.env') });

const ENV = process.env.NODE_ENV || 'development';
const MONGODB_URI = process.env.MONGODB_URI || 'mongodb://localhost:27017/cajajper';
const RECIBO_DIGITAL_URI = process.env.RECIBO_DIGITAL_URI || 'mysql://root:root@localhost:3306/recibosdigitales';
const EXPEDIENTES_URI = process.env.EXPEDIENTES_URI || 'mysql://root:root@localhost:3306/expedientes';
const NOVEDADES_URI = process.env.NOVEDADES_URI || 'mysql://root:root@localhost:3306/cajajper_extranet';
const JWT_SECRET_KEY = process.env.JWT_SECRET_KEY || 'JWT_SECRET_KEY';

const SERVER_HOST = process.env.SERVER_HOST || '';

const PORT: number = ((process.env.PORT as unknown) as number) || 80;
const SMTP_SERVER = process.env.SMTP_SERVER || '';
const SMTP_SERVER_PORT: number = ((process.env.SMTP_SERVER_PORT as unknown) as number) || 25;
const SMTP_AUTH_EMAIL = process.env.SMTP_AUTH_EMAIL || '';
const SMTP_AUTH_PASSWORD = process.env.SMTP_AUTH_PASSWORD || '';

const SECRET = process.env.SECRET || '';
const CRYPTO_PREFIX = process.env.CRYPTO_PREFIX || '';
const CRYPTO_SUFFIX = process.env.CRYPTO_SUFFIX || '';
const API_SHARED_SECRET = process.env.API_SHARED_SECRET || '';

export interface ConfigInterface {
  API_SHARED_SECRET: string;
  ENV: string;
  JWT_SECRET_KEY: string;
  MONGODB_URI: string;
  PORT: number;
  SERVER_HOST: string;
  SMTP_SERVER: string;
  SMTP_SERVER_PORT: number;
  SMTP_AUTH_EMAIL: string;
  SMTP_AUTH_PASSWORD: string;
  SECRET: string;
  CRYPTO_PREFIX: string;
  CRYPTO_SUFFIX: string;
  RECIBO_DIGITAL_URI: string;
  EXPEDIENTES_URI: string;
  NOVEDADES_URI: string;
}

const Config: ConfigInterface = {
  API_SHARED_SECRET,
  CRYPTO_PREFIX,
  CRYPTO_SUFFIX,
  ENV,
  EXPEDIENTES_URI,
  JWT_SECRET_KEY,
  MONGODB_URI,
  NOVEDADES_URI,
  PORT,
  RECIBO_DIGITAL_URI,
  SECRET,
  SERVER_HOST,
  SMTP_AUTH_EMAIL,
  SMTP_AUTH_PASSWORD,
  SMTP_SERVER,
  SMTP_SERVER_PORT
};

export default Config;
