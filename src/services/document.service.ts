import { getConnection, ObjectID } from 'typeorm';

import BaseService from './base.service';
import BenefitType from '~/entities/documents/benefit-type';
import Document from '~/entities/documents/document.entity';
import Office from '~/entities/documents/office';
import RequestType from '~/entities/documents/request-type';
import ApplicantsRepository from '~/repositories/applicants.repository';
import MovementRepository from '~/repositories/movements.repository';


export default class DocumentService extends BaseService {
    private movementsRepository = getConnection('expedientes').getCustomRepository(MovementRepository);
    private applicantsRepository = getConnection('expedientes').getCustomRepository(ApplicantsRepository);

    public async search(q: number): Promise<Document[]> {
        const query = getConnection('expedientes').getRepository(Document)
            .createQueryBuilder('Document')
            .leftJoinAndSelect('solicitantes', "ApplicantFilter", "ApplicantFilter.expediente_id = Document.id")
            .leftJoinAndMapOne('Document.office', Office, "Office", "Office.id = Document.area_id")
            .leftJoinAndMapOne('Document.request_type', RequestType, "RequestType", "RequestType.id = Document.request_type")
            .leftJoinAndMapOne('Document.benefit_type', BenefitType, "BenefitType", "BenefitType.id = Document.benefit_type")
            .where([{ id: q }])
            .orWhere('ApplicantFilter.documento=:q', { q })
            .andWhere('Document.pre_carga=false')
            .limit(100);


        const results = await query.getMany();



        const ret = Promise.all(results.map(async (item) => {
            item.applicants = await this.applicantsRepository.find({
                take: 20,
                where: {
                    document_id: item.id
                }
            });
            item.last_movements = await this.movementsRepository.find({
                order: { modified: 'DESC' },
                take: 3,
                where: {
                    document_id: item.id,
                    status: 2
                }
            });

            return item;
        }));


        return ret;

    }

    public async viewByIds(q: [number]): Promise<Document[]> {
        const query = getConnection('expedientes').getRepository(Document)
            .createQueryBuilder('Document')
            .leftJoinAndSelect('solicitantes', "ApplicantFilter", "ApplicantFilter.expediente_id = Document.id")
            .leftJoinAndMapOne('Document.office', Office, "Office", "Office.id = Document.area_id")
            .leftJoinAndMapOne('Document.request_type', RequestType, "RequestType", "RequestType.id = Document.request_type")
            .leftJoinAndMapOne('Document.benefit_type', BenefitType, "BenefitType", "BenefitType.id = Document.benefit_type")
            .where([{ id: q }])
            .andWhere('Document.pre_carga=false')
            .limit(100);


        const results = await query.getMany();



        const ret = Promise.all(results.map(async (item) => {
            item.applicants = await this.applicantsRepository.find({
                take: 20,
                where: {
                    document_id: item.id
                }
            });
            item.last_movements = await this.movementsRepository.find({
                order: { modified: 'DESC' },
                take: 3,
                where: {
                    document_id: item.id,
                    status: 2
                }
            });

            return item;
        }));


        return ret;

    }


    public async view(q: number): Promise<Document | null> {
        const query = getConnection('expedientes').getRepository(Document)
            .createQueryBuilder('Document')
            .leftJoinAndSelect('solicitantes', "ApplicantFilter", "ApplicantFilter.expediente_id = Document.id")
            .leftJoinAndMapOne('Document.office', Office, "Office", "Office.id = Document.area_id")
            .leftJoinAndMapOne('Document.request_type', RequestType, "RequestType", "RequestType.id = Document.request_type")
            .leftJoinAndMapOne('Document.benefit_type', BenefitType, "BenefitType", "BenefitType.id = Document.benefit_type")
            .where([{ id: q }])
            .andWhere('Document.pre_carga=false')
            .limit(100);


        const item = await query.getOne();
        if (!item) {
            return null;
        }

        item.applicants = await this.applicantsRepository.find({
            take: 20,
            where: {
                document_id: item.id
            }
        });
        item.last_movements = await this.movementsRepository.find({
            order: { modified: 'DESC' },
            take: 3,
            where: {
                document_id: item.id,
                status: 2
            }
        });




        return item;

    }





}