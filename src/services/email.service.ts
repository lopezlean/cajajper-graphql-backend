import * as nodemailer from 'nodemailer';
import Mail from 'nodemailer/lib/mailer';
import SMTPTransport from 'nodemailer/lib/smtp-transport';
import * as pug from 'pug';

import Config from '~/config';

interface EmailServiceInterface {
  to: string;
  subject: string;
  layout: string;
  template: string;
  context: pug.LocalsObject;
  attachments?: Mail.Attachment[];
  from?: string;
}

const options: SMTPTransport.Options = {
  auth: {
    pass: Config.SMTP_AUTH_PASSWORD,
    user: Config.SMTP_AUTH_EMAIL
  },
  host: Config.SMTP_SERVER,
  port: Config.SMTP_SERVER_PORT,
  secure: false
};
//
export default class EmailService {
  public baseUrl = Config.SERVER_HOST;

  private transporter: nodemailer.Transporter = nodemailer.createTransport(options);

  send({
    to,
    subject,
    layout,
    template,
    context = {},
    attachments = [],
    from = Config.SMTP_AUTH_EMAIL
  }: EmailServiceInterface): Promise<string> {
    const promise: Promise<string> = new Promise((resolver, rejected) => {
      // Context default
      const contextDefault = {
        appStoreUrl: 'http://appstore',
        baseUrl: this.baseUrl,
        playStoreUrl: 'http://appstore',
        ...context
      };
      // Process template
      const templateSrc = `${process.cwd()}/src/emails/templates/${template}.pug`;
      const templateHTML = this.proccessPug(templateSrc, contextDefault);
      // Process layout
      const layoutSrc = `${process.cwd()}/src/emails/layouts/${layout}.pug`;
      const contextLayout = {
        ...contextDefault,
        content: templateHTML
      };
      const layoutHTML = this.proccessPug(layoutSrc, contextLayout);
      // Send email
      const mailOptions: Mail.Options = {
        attachments,
        from,
        html: layoutHTML,
        subject,
        to
      };

      this.transporter.sendMail(mailOptions, function (error, info : {response:string}) {
        if (error) {
          rejected(error);
        } else {
          resolver(`Email sent: ${info.response}`);
        }
      });
    });

    return promise;
  }
  private proccessPug(template: string, context: pug.LocalsObject): string {
    const compiledTemplate: pug.compileTemplate = pug.compileFile(template);

    const html: string = compiledTemplate(context);
    return html;
  }
}
