import * as fs from 'fs';
import { v4 as uuid } from 'uuid';

export default class LocalFileService {
  private host = process.env.LOCAL_SERVER_HOST;
  private folder = process.env.LOCAL_SERVER_FOLDER;

  copyToPathDoc(file, type = 'docs'): string {
    const imageDir = `${this.folder}/${type}`;
    if (!fs.existsSync(imageDir)) {
      fs.mkdirSync(imageDir);
    }
    console.log(file);
    file.name = `${uuid()}_${file.name}`;
    const path = `${imageDir}/${file.name}`;
    const base64Data = file.content;
    fs.writeFile(`./${path}`, base64Data, 'base64', (err) => {
      console.log(err);
    });

    return `${this.host}${path}`;
  }

  copyToPathPDF(file, type = 'pdf'): string {
    const imageDir = `${this.folder}/${type}`;
    if (!fs.existsSync(imageDir)) {
      fs.mkdirSync(imageDir);
    }
    console.log(Object.keys(file));
    file.name = `${uuid()}_${file.name}`;
    const path = `${imageDir}/${file.name}`;
    // console.log(file.content)
    const base64Data = new Buffer(
      file.content.replace(/^data:application\/\w+;base64,/, ''),
      'base64'
    );
    fs.writeFile(`./${path}`, base64Data, 'base64', (err) => {
      console.log(err);
    });

    return `${this.host}${path}`;
  }

  copyToImage(file, type = 'images'): string {
    const imageDir = `${this.folder}/${type}`;
    if (!fs.existsSync(imageDir)) {
      fs.mkdirSync(imageDir);
    }
    console.log(Object.keys(file));
    file.name = `${uuid()}_${file.name}`;
    const path = `${imageDir}/${file.name}`;
    const base64Data = new Buffer(file.content.replace(/^data:image\/\w+;base64,/, ''), 'base64');
    fs.writeFile(`./${path}`, base64Data, 'base64', (err) => {
      console.log(err);
    });

    return `${this.host}${path}`;
  }

  copyAudioToPath(file): string {
    const audioDir = `${this.folder}/audio`;
    if (!fs.existsSync(audioDir)) {
      fs.mkdirSync(audioDir);
    }
    // console.log(file);
    file.name = `${uuid()}_${file.name}`;
    const path = `${audioDir}/${file.name}`;
    const base64Data = file.content.replace('data:audio/mp3;base64,', '');
    fs.writeFile(`./${path}`, base64Data, 'base64', (err) => {
      console.log(err);
    });
    return `${this.host}${path}`;
  }
  // Unlink file
  async unlink(path: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      const pathReplace = path.replace('http://localhost:4000/files', '');
      console.log(pathReplace);
      fs.unlink(`${this.folder}${pathReplace}`, (err) => {
        if (err) {
          console.log('error unlink', err);
          resolve(false);
        }
        console.log(`successfully deleted ${path}`);
        resolve(true);
      });
    });
  }
  existFile(path: string): boolean {
    return fs.existsSync(path);
  }
}
