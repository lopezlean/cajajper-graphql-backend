import { getConnection } from 'typeorm';

import BaseService from './base.service';
import News from '~/entities/news/news.entity';
import NewsRepository from '~/repositories/news.repository';

export default class NewsService extends BaseService {

    private movementsRepository = getConnection('novedades').getCustomRepository(NewsRepository);


    public async last(): Promise<News[]> {
        return this.movementsRepository.find({
            order: {
                id: 'DESC'
            },
            take: 50,
            where: {
                status: true
            }
        })
    }
}
