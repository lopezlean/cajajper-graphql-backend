import { getConnection } from 'typeorm';

import BaseService from './base.service';
import Office from '~/entities/documents/office';
import OfficesRepository from '~/repositories/offices.repository';

export default class OfficeService extends BaseService {

    private officesRepository = getConnection('expedientes').getCustomRepository(OfficesRepository);

    public get(id: number): Promise<Office | undefined> {
        return this.officesRepository.findOne({ id });
    }

}