import LocalFileService from './local-file.service';

export default class FileService {
  static getInstance() {
    return new LocalFileService();
  }
}
