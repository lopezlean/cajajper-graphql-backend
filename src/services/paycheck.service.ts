import { AuthenticationError } from 'apollo-server';
import * as fs from "fs";
import * as path from 'path';
import fetch from 'node-fetch';
import { ObjectID } from 'mongodb';
import { getConnection } from 'typeorm';

import BaseService from './base.service';
import PaycheckList from '~/entities/paycheck/paycheck_list.entity';
import PaycheckUser from '~/entities/paycheck/paycheck-user.entity';
import Paycheck from '~/entities/paycheck/paycheck.entity';
import PaycheckUserRepository from '~/repositories/paycheck-users.repository';
import PaycheckValidationRepository from '~/repositories/paycheck-validation.repository';
import PaychecksRepository from '~/repositories/paychecks.repository';
import UserRepository from '~/repositories/user.repository';
import { shared_encode } from '~/helpers/security/token.helper';

export default class PaycheckService extends BaseService {
  private userRepository = getConnection().getCustomRepository(UserRepository);
  private paycheckUserRepository = getConnection('recibo').getCustomRepository(
    PaycheckUserRepository
  );
  private paychecksRepository = getConnection('recibo').getCustomRepository(PaychecksRepository);
  private paycheckValidationRepository = getConnection('recibo').getCustomRepository(PaycheckValidationRepository);

  public async getUser(id: number): Promise<PaycheckUser | undefined> {
    return await this.paycheckUserRepository.findOne({
      id
    });
  }

  public async list(userId: ObjectID, accountId: number): Promise<PaycheckList[]> {
    const user = await this.userRepository.findOneOrFail({ where: { _id: new ObjectID(userId) } });
    if (
      !user.paycheck_users ||
      !user.paycheck_users.ids ||
      !user.paycheck_users.ids.includes(accountId)
    ) {
      throw new AuthenticationError('Cuenta inválida');
    }

    const paycheckUser = await this.paycheckUserRepository.findOneOrFail({
      id: accountId
    });
    const full_account_number = `CTA.ING.${paycheckUser.full_account_number}`;

    const ret = await this.paychecksRepository
      .createQueryBuilder()
      .select('Paycheck.mesLiquidacion', 'month')
      //.addSelect('Paycheck.descripcionCuenta', 'full_account_numer')
      .addSelect('Paycheck.anioLiquidacion', 'year')
      //.addSelect('Paycheck.tipoLiquidacion', 'type')
      //.addSelect('Paycheck.idTipoArchivo', 'file_type_id')
      //.addSelect('Paycheck.idAnexo', 'annex_id')

      .where({ full_account_number })

      .groupBy('Paycheck.anioLiquidacion')
      .addGroupBy('Paycheck.mesLiquidacion')
      //.addGroupBy('Paycheck.tipoLiquidacion')
      //.addGroupBy('Paycheck.idTipoArchivo')
      //.addGroupBy('Paycheck.idAnexo')
      //.addGroupBy('Paycheck.descripcionCuenta')
      .orderBy('Paycheck.anioLiquidacion', 'DESC')
      .addOrderBy('Paycheck.mesLiquidacion', 'DESC')
      .getRawMany();

    //const ret =  this.paychecksRepository.createQueryBuilder().where({ full_account_number }).getSql();


    const map: PaycheckList[] = (ret.map(
      async (item: Paycheck): Promise<PaycheckList> => {
        const innerPaychecks = await this.paychecksRepository
          .createQueryBuilder()
          .select('Paycheck.idArchivo', 'file_id')
          .addSelect('Paycheck.idTipoArchivo', 'file_type_id')
          .addSelect('Paycheck.mesLiquidacion', 'month')
          .addSelect('Paycheck.anioLiquidacion', 'year')
          .addSelect('Paycheck.tipoLiquidacion', 'type')
          .addSelect('Paycheck.idAnexo', 'annex_id')
          .addSelect('Paycheck.numeroDocumento', 'national_identification_number')
          .addSelect('Paycheck.apellidoNombres', 'full_name')
          .addSelect('Paycheck.numeroCuenta', 'account_number')
          .addSelect('Paycheck.idSucursal', 'branch_office_id')
          .addSelect('Paycheck.descripcionCuenta', 'full_account_number')
          .addSelect('Paycheck.periodoLiquidacion', 'period')

          .where({ full_account_number, month: item.month, year: item.year })

          .groupBy('Paycheck.anioLiquidacion')
          .addGroupBy('Paycheck.mesLiquidacion')
          .addGroupBy('Paycheck.descripcionCuenta')
          .addGroupBy('Paycheck.tipoLiquidacion')
          .addGroupBy('Paycheck.idTipoArchivo, Paycheck.idAnexo, Paycheck.numeroCuenta,Paycheck.descripcionCuenta, Paycheck.periodoLiquidacion, Paycheck.idSucursal, Paycheck.apellidoNombres, Paycheck.numeroDocumento, Paycheck.idArchivo')
          .orderBy('Paycheck.anioLiquidacion', 'DESC')
          .addOrderBy('Paycheck.mesLiquidacion', 'DESC')
          .getRawMany();

        const paycheckList = new PaycheckList();
        paycheckList.month = item.month;
        paycheckList.year = item.year;

        paycheckList.paycheck = innerPaychecks.map((pItem) => this.paychecksRepository.create(pItem)
        ) as unknown as [Paycheck | undefined];
        return paycheckList;
      }
    ) as unknown) as PaycheckList[];
    return Promise.all(map);
  }



  public async view(
    userId: ObjectID,
    accountId: number,
    year: number,
    month: number,
    type: string,
    annex_id: number,
    file_type_id: number
  ): Promise<Paycheck[]> {

    await this.checkPaycheckAccountOwnership(userId, accountId)

    const paycheckUser = await this.paycheckUserRepository.findOneOrFail({
      id: accountId
    });
    const full_account_number = `CTA.ING.${paycheckUser.full_account_number}`;


    return this.paychecksRepository
      .createQueryBuilder()
      .where({
        annex_id,
        file_type_id,
        full_account_number,
        month,
        type,
        year
      })
      .leftJoinAndMapOne("Paycheck.paycheck_code", "detallearchivo_codigos", "PaycheckCode", "Paycheck.numeroCodigo = PaycheckCode.numeroCodigo")
      .leftJoinAndMapOne("Paycheck.paycheck_annex", "anexos", "PaycheckAnnex", "Paycheck.idAnexo = PaycheckAnnex.id")

      .orderBy('PaycheckCode.type', 'DESC')
      .addOrderBy('Paycheck.numeroCodigo', 'ASC')
      .getMany();
  }

  public async viewPDF(
    userId: ObjectID,
    accountId: number,
    year: number,
    month: number,
    type: string,
    annex_id: number,
    file_type_id: number
  ): Promise<string> {

    await this.checkPaycheckAccountOwnership(userId, accountId)

    await this.paycheckUserRepository.findOneOrFail({
      id: accountId
    });



    const paycheckValidationQuery = this.paycheckValidationRepository
      .createQueryBuilder()
      .where({
        annex_id,
        file_type_id,
        month,
        type,
        year
      }
      );

    const paycheckValidation = await paycheckValidationQuery.getOne();



    if (paycheckValidation) {
      const fileName = `/files/${String(userId)}-${paycheckValidation.guid}.pdf`;
      const PATH = `${process.cwd()}${fileName}`;

      if (fs.existsSync(PATH)) {
        return fileName;
      }
      const token = shared_encode({
        id: paycheckValidation.guid
      });
      console.log({ token })

      const response = await fetch(`https://recibodigital.cajajper.gov.ar/layout/mobile-app/recibo-digital-pdf.php?u=${paycheckValidation.guid}`, {
        headers: {
          'Authorization': `BEARER ${token}`
        }
      });

      if (response.headers.get('content-type') !== 'application/pdf') {
        console.log('error');
        return '';
      }




      const fileStream = fs.createWriteStream(PATH);
      await new Promise((resolve, reject) => {
        response.body.pipe(fileStream);
        response.body.on("error", reject);
        fileStream.on("finish", resolve);
      });

      return fileName;
    }
    return '';
  }


  private async checkPaycheckAccountOwnership(userId: ObjectID, accountId: number) {
    const user = await this.userRepository.findOneOrFail({ where: { _id: new ObjectID(userId) } });
    if (
      !user.paycheck_users ||
      !user.paycheck_users.ids ||
      !user.paycheck_users.ids.includes(accountId)
    ) {
      throw new AuthenticationError('Cuenta inválida');
    }

    return user;

  }

}
