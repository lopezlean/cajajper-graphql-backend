import 'reflect-metadata';

import { ApolloError, AuthenticationError } from 'apollo-server';
import axios from 'axios';
import { sha512 } from 'js-sha512';
import { ObjectID } from 'mongodb';
import { Service } from 'typedi';
import { getMongoManager, getConnection } from 'typeorm';

import CryptrHelper from '../helpers/security/cryptr.helper';
import { encode, TokenInterface } from '../helpers/security/token.helper';
import BaseService from './base.service';
import EmailService from './email.service';
import FileService from './file.service';
import Auth from '~/entities/auth.entity';
import Document from '~/entities/documents/document.entity';
import PaycheckUserList from '~/entities/paycheck-user-list.entity';
import PaycheckUser from '~/entities/paycheck/paycheck-user.entity';
import User from '~/entities/user.entity';
import BadRequestException from '~/exceptions/bad-request.exception';
import InternalException from '~/exceptions/internal.exception';
import { getActivationCode } from '~/helpers/security/get-activation-code.helper';
import PaycheckUserRepository from '~/repositories/paycheck-users.repository';
import UserRepository from '~/repositories/user.repository';

@Service()
export default class UserService extends BaseService {
  private cryptrHelper = new CryptrHelper();

  private readonly fileService;
  private userRepository = getConnection().getCustomRepository(UserRepository);
  private PaycheckUserRepository = getConnection('recibo').getCustomRepository(
    PaycheckUserRepository
  );
  constructor() {
    super();
    this.fileService = FileService.getInstance();
  }


  public async get(id: ObjectID): Promise<User> {
    const user = await this.userRepository.findById(id);
    if (!user) {
      throw new BadRequestException(`user don't exist`);
    }
    return user;
  }
  /**
   * Returns token from auth
   *
   * @remarks
   * This method is part of the {@link core-library#User | User subsystem}.
   *
   * @param username - The first input string
   * @param password - The second input string
   * @returns String whit user encode in a token
   *
   * @beta
   */
  public async login(email: string, password: string): Promise<Auth> {
    const user: User | undefined = await this.userRepository.findOne({ email });

    if (user) {
      const passwordOk: boolean = CryptrHelper.compare(password, user.password);
      if (!passwordOk) {
        throw new AuthenticationError(`user or password don't exist`);
      }
      if (!user.active) {
        throw new ApolloError(`El usuario no esta activo`, '400', [
          { constraints: { isActive: false } }
        ]);
      }

      return this.getAuthForUser(user);
    }

    throw new AuthenticationError(`user or password don't exist`);
  }


  public async register(user: User, active: boolean = false): Promise<User> {
    user.password = CryptrHelper.encript(user.password);
    user.active = active;
    const activationCode = String(getActivationCode());
    user.activation_code = CryptrHelper.encript(activationCode);

    const toSave = this.userRepository.create(user);
    const userCreated = await getMongoManager().save(toSave);
    if (!user.active) {
      void this.sendWelcomeEmail(userCreated, activationCode);
    }

    return userCreated;
  }

  public async resendActivationCode(email: string): Promise<User> {
    const user = await this.userRepository.findOne({ email });
    if (!user) {
      throw new BadRequestException(`Usuario invalido`);
    }

    if (user?.active) {
      throw new BadRequestException(`Usuario ya activado`);
    }
    const activationCode = String(getActivationCode());
    user.activation_code = CryptrHelper.encript(activationCode);
    const userCreated = await getMongoManager().save(user);
    if (!userCreated) {
      throw new InternalException(`Ocurrio un error ha actualizar el usuario`);
    }

    void this.sendWelcomeEmail(userCreated, activationCode);

    return userCreated;
  }

  public async activateUser(email: string, activationCode: number): Promise<Auth> {
    const user = await this.userRepository.findOne({ email });
    if (!user) {
      throw new BadRequestException(`Usuario invalido`);
    }
    if (user.active) {
      throw new BadRequestException(`Usuario invalido`);
    }

    const activationCodeString = String(activationCode);
    const activationCodeEncripted = CryptrHelper.encript(activationCodeString);

    if (user.activation_code !== activationCodeEncripted) {
      throw new BadRequestException(`Código inválido`);
    }
    user.active = true;
    const userCreated = await getMongoManager().save(user);

    if (!userCreated) {
      throw new InternalException(`Ocurrio un error ha actualizar el usuario`);
    }

    return this.getAuthForUser(userCreated);
  }

  public async facebookLogin(token: string): Promise<Auth> {
    if (!token) {
      throw new AuthenticationError('No facebook token provided');
    }

    const facebookUrl = `https://graph.facebook.com/me?access_token=${token}&fields=name,email,first_name,last_name`;
    try {
      const facebookUser = (await (await axios.get(facebookUrl)).data) as {
        error: boolean;
        email: string;
      };

      if (facebookUser.error) {
        throw new AuthenticationError('Token inválido');
      }
      const user = await this.userRepository.findOne({ email: facebookUser.email });

      //user already exist, we trust in facebook and use it
      if (user) {
        return this.getAuthForUser(user);
      }

      const userData = this.userRepository.create(facebookUser);
      const createdUser = await this.register(userData, true);
      return this.getAuthForUser(createdUser);

    } catch (e) {
      throw new AuthenticationError(e);
    }
  }

  public async paycheckAssociate(
    email: string,
    password: string,
    userId: ObjectID
  ): Promise<boolean> {
    try {
      const databasePaycheckUser = await this.paycheckCheckUser(email, password);
      return this.paycheckAssociateAddToSet(userId, databasePaycheckUser.id);
    } catch (e) {
      throw new AuthenticationError(e);
    }
  }

  public async paycheckLogin(email: string, password: string): Promise<Auth> {
    try {
      const databasePaycheckUser = await this.paycheckCheckUser(email, password);

      const userExists = await this.userRepository.findOne({ email });
      const paycheckUserList = new PaycheckUserList();
      paycheckUserList.ids = [databasePaycheckUser.id];
      if (!userExists) {
        const userData = this.userRepository.create({
          email,
          password,
          paycheck_users: paycheckUserList
        });

        const createdUser = await this.register(userData, true);
        return this.getAuthForUser(createdUser);

      }

      if (!userExists.paycheck_users || userExists.paycheck_users.ids.length < 1) {
        userExists.paycheck_users = paycheckUserList;
        await this.userRepository.save(userExists);
      } else {
        await this.paycheckAssociateAddToSet(
          userExists.id,
          databasePaycheckUser.id
        );
      }
      return this.getAuthForUser(userExists);
    } catch (error) {
      throw new Error(error);
    }
  }

  public async addToFavorites(userId: ObjectID, documentId: number): Promise<boolean> {

    const result = await this.userRepository.updateOne(
      { _id: new ObjectID(userId) },
      {
        $addToSet: { 'favorite_documents.ids': documentId } // atomic update, only add if the id is not in the ids array
      }
    );
    console.log(result.result);
    return result.result.ok === 1;
  }


  public async removeFromFavorites(userId: ObjectID, documentId: number): Promise<boolean> {

    const result = await this.userRepository.updateOne(
      { _id: new ObjectID(userId) },
      {
        $pull: { 'favorite_documents.ids': documentId } // atomic update, only add if the id is not in the ids array
      }
    );
    console.log(result.result);
    return result.result.ok === 1;
  }

  private async paycheckAssociateAddToSet(
    userId: ObjectID,
    PaycheckUserId: number
  ): Promise<boolean> {

    const result = await this.userRepository.updateOne(
      { _id: new ObjectID(userId) },
      {
        $addToSet: { 'paycheck_users.ids': PaycheckUserId } // atomic update, only add if the id is not in the ids array
      }
    );
    console.log(result.result);
    return result.result.ok === 1;
  }
  private async paycheckCheckUser(
    email: string,
    password: string
  ): Promise<PaycheckUser> {
    try {
      const databasePaycheckUser = await this.PaycheckUserRepository.findOne({
        email
      });

      if (!databasePaycheckUser) {
        throw new AuthenticationError('Usuario o contraseñas incorrectos');
      }
      const passwordHash = sha512(password);
      const userPassword = sha512(`${passwordHash}${databasePaycheckUser.salt}`);

      if (userPassword !== databasePaycheckUser.password) {
        throw new AuthenticationError('Usuario o contraseñas incorrectos');
      }

      return databasePaycheckUser;
    } catch (error) {
      throw new AuthenticationError(error);
    }
  }
  private async sendWelcomeEmail(user: User, activationCode: string) {
    const host: string = process.env.FRONTEND_SERVER_HOST || 'https://graphql.cajajper.gov.ar';
    const token = encode(user as TokenInterface);
    this.logger.debug(`token generated confirm account: ${token}`);
    const link = `${host}confirm-account/${token}`;
    // Send email
    const emailService = new EmailService();
    try {
      await emailService.send({
        context: {
          activationCode,
          footer: 'people',
          link
        },
        layout: 'default',
        subject: 'Bienvenido a la Caja de Jubilaciones y Pensiones de Entre Ríos',
        template: 'welcome',
        to: user.email
      });
    } catch (e) {
      console.log(e);
    }
  }
  private getAuthForUser(user: User): Auth {
    const auth = new Auth();
    auth.token = this.getTokenForUser(user);
    auth.id = user.id;
    auth.email = user.email;
    return auth;
  }
  private getTokenForUser(user: User): string {
    if (!user) {
      throw new InternalException(`user token error`);
    }

    const token = encode(user as TokenInterface);

    return token;
  }
}
