import { Context } from 'apollo-server-core';
import { ExpressContext } from 'apollo-server-express';
import { ForbiddenError } from 'type-graphql';
import Container, { ContainerInstance } from 'typedi';
import {  ObjectID } from 'mongodb';

import { decode, TokenInterface } from '../helpers/security/token.helper';
import RestApiCajajper from '~/dataSources/restApicajajper';

export const context = ({ req }: ExpressContext): Context => {
  const authorization: string =
    req && req.headers ? (req.headers.authorization as string) : ('' as string);
  if (!authorization) {
    const requestId = Math.floor(Math.random() * Number.MAX_SAFE_INTEGER); // uuid-like
    const container = Container.of(requestId); // get the scoped container
    const context = {
      container,
      requestId
    }; // create fresh context object
    container.set('context', context); // place context or other data in container
    return context;
  }
  let decoded: TokenInterface = {
    email: '',
    id: new ObjectID(1)
  };
  try {
    // Bearer parse:
    // Authorization: Bearer herethekey
    if (authorization && authorization.length > 0) {
      const parsedAuthorization: string[] = authorization.split(' ');
      const token = parsedAuthorization[1];
      decoded = decode(token);
    } else {
      throw new ForbiddenError();
    }
  } catch (error) {
    throw new ForbiddenError();
  }

  const loggedInUser: TokenInterface = {
    email: decoded.email,
    id: decoded.id
  };
  const requestId = Math.floor(Math.random() * Number.MAX_SAFE_INTEGER); // uuid-like
  const container = Container.of(requestId); // get scoped container
  const context = {
    container,
    headers: {
      authorization
    },
    loggedInUser,
    requestId
  }; // create our context

  container.set('context', context); // place context or other data in container
  return context;
};

export default interface ContextInterface {
  requestId: number;
  loggedInUser: TokenInterface;
  container: ContainerInstance;
  dataSources: {
    restApiCajajper: RestApiCajajper;
  };
  headers: any;
}
