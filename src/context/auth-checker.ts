import  Context from '../context/context';
import { AuthChecker } from 'type-graphql';

// create auth checker function
export const authChecker: AuthChecker<Context> = ({ context: { loggedInUser } }) => {

    return loggedInUser !== undefined;
};
