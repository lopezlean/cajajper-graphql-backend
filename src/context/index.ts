const { AuthenticationError } = require('apollo-server-express');
const jwt = require('jsonwebtoken');
import Container, { ContainerInstance } from 'typedi';


import TokenHelper from '../helpers/security/token.helper';

const JWT_SECRET_KEY  = TokenHelper.secret;

const context = ({ req }) => {
  try {
    const authorization = ( req && req.headers) ? req.headers.authorization : undefined;

    if (!authorization) {
      return { };
    }
    const token = authorization.split(' ')[1];
    const decoded = jwt.verify(token, JWT_SECRET_KEY);

    const loggedInUser =  { id: decoded.id, email: decoded. email };

    const requestId = Math.floor(Math.random() * Number.MAX_SAFE_INTEGER); // uuid-like
    const container = Container.of(requestId); // get scoped container
    const ctx = {
      loggedInUser,
      requestId,
      container
    }; // create our context

    container.set('context', ctx); // place context or other data in container
    return ctx;
  } catch (error) {
    throw new AuthenticationError('invalid token');
  }
};

module.exports = context;
