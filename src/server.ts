import 'reflect-metadata';

import { ApolloServer } from 'apollo-server-express';
//import bodyParser from 'body-parser';
import express from 'express';
import http from 'http';
import logger from 'morgan';
import { buildSchema, ResolverData } from 'type-graphql';

import { ping } from './api-rest/ping/ping';
import Config from './config';
import { authChecker } from './context/auth-checker';
import ContextInterface, { context } from './context/context';
import Database from './database';
import dataSources from './dataSources';
import Resolvers from './resolvers';

// API REST

// Creates and configures an ExpressJS web server.
class Server {
  app: express.Application = express();
  server: http.Server = new http.Server();
  apollo: ApolloServer;
  database = new Database();

  constructor() {
    void this.initApollo();
  }

  middleware(): void {
    this.app.use(
      // eslint-disable-next-line @typescript-eslint/no-unsafe-call
      logger('dev', {
        skip: (req: { body: { operationName: string } }) => {
          return req.body && req.body.operationName === 'IntrospectionQuery'; // ignore introspection queries from playground
        }
      })
    );

    // this.app.use(cors());
    //this.app.use(bodyParser.json({ limit: '50mb' }));
    //this.app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
    this.app.use('/files', express.static('files'));
    this.apollo.applyMiddleware({
      app: this.app,
      cors: {
        origin: '*'
      },
      path: '/graphql'
    });
  }

  routes(): void {
    /* This is just to get up and running, and to make sure what we've got is
     * working so far. This function will change when we start to add more
     * API endpoints */
    const router = express.Router();
    this.app.disable('etag');
    this.app.use('/', router);

    // configure engine
    this.app.set('view engine', 'pug');
    //this.app.set('views', path.join(__dirname, '/api-rest/views'));

    this.app.get('/ping', ping);
  }

  async start(cb = () => null): Promise<void> {
    const port = Config.PORT || 4000;
    console.log({ Config }, 'config');
    await this.database.connect();
    this.server = this.app.listen({ port }, () => {
      console.log(`🔥 Server running on http://localhost:${port}/graphql`);
      cb();
    });
  }

  stop(cb = () => null): void {
    //if (this.server) {
    this.server.close(cb);
    //}
  }

  private async initApollo(): Promise<void> {
    try {
      const schema = await buildSchema({
        authChecker,
        container: ({ context }: ResolverData<ContextInterface>) => context.container,
        resolvers: Resolvers
      });
      this.apollo = new ApolloServer({
        context,
        dataSources,
        // Enables and disables schema introspection. Disabled in production by default.
        introspection: true,

        /*formatResponse: (response: any, { context }: ResolverData<Context>) => {
          // remember to dispose the scoped container to prevent memory leaks
          Container.reset(context.requestId);
          return response;
        },*/
        playground: true,
        schema
      });
      this.apollo.installSubscriptionHandlers(this.server);
      this.middleware();
      this.routes();
    } catch (e) {
      console.log(e);
    }
  }
}

export default Server;
