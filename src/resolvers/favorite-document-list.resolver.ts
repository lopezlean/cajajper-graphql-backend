import { Resolver, FieldResolver, Root } from 'type-graphql';

import Document from '~/entities/documents/document.entity';
import FavoriteDocumentList from '~/entities/favorite-document-list.entity';
import DocumentService from '~/services/document.service';




@Resolver(FavoriteDocumentList)
export default class FavoriteDocumentListResolver {
  private documentService = new DocumentService;


  @FieldResolver(() => FavoriteDocumentList, { nullable: true })
  async documents(@Root() item: Promise<FavoriteDocumentList>): Promise<Promise<Document | undefined>[] | null> {
    const i = await item;
    console.log({i});
    if (!i.ids) {
      return null;
    }
    const ret = i.ids.map(async (item) => {
      const document = await this.documentService.view(item);
      if (document) {
        return document;
      }

    });

    return ret;

  }

}
