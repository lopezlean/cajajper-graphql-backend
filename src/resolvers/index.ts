import DocumentResolver from './document.resolver';
import FavoriteDocumentListResolver from './favorite-document-list.resolver';
import MovementResolver from './movement.resolver';
import NewsResolver from './news.resolver';
import PaycheckResolver from './paycheck.resolver';
import PaycheckUserListResolver from './payckeck-user-list.resolver';
import UserResolver from './user.resolver';

export default [UserResolver, DocumentResolver, PaycheckResolver, NewsResolver, MovementResolver, PaycheckUserListResolver, FavoriteDocumentListResolver] as const;
