import { Resolver, FieldResolver, Root } from 'type-graphql';

import Movement from '~/entities/documents/movement';
import Office from '~/entities/documents/office';
import OfficeService from '~/services/office.service';

@Resolver(Movement)
export default class MovementResolver {
  private officeService = new OfficeService;


  @FieldResolver(() => Office)
  async office(@Root() item: Promise<Movement>): Promise<Office> {
    const i = await item;
    const ret = await this.officeService.get(i.office_id);

    if (!ret) {
      const newOffice = new Office;
      newOffice.name = 'Caja de Jubilaciones Y Pensiones';
      return newOffice;
    }

    return ret;

  }

}
