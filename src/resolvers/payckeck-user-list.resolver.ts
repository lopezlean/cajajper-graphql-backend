import { Resolver, FieldResolver, Root } from 'type-graphql';


import PaycheckUserList from '~/entities/paycheck-user-list.entity';
import PaycheckUser from '~/entities/paycheck/paycheck-user.entity';

import PaycheckService from '~/services/paycheck.service';

@Resolver(PaycheckUserList)
export default class PaycheckUserListResolver {
  private paycheckService = new PaycheckService;


  @FieldResolver(() => PaycheckUserList, { nullable: true })
  async users(@Root() item: Promise<PaycheckUserList>): Promise<Promise<PaycheckUser | undefined>[] | null> {
    const i = await item;

    if (!i.ids) {
      return null;
    }
    const ret = i.ids.map(async (item) => {
      const user = await this.paycheckService.getUser(item);
      console.log({user});
      if (user) {
        return user;
      }

    });

    return ret;

  }

}
