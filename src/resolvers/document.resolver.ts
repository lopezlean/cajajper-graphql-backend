import { Resolver, Query, Arg, Args, Ctx, Int } from 'type-graphql';

import Document from '~/entities/documents/document.entity';
import DocumentService from '~/services/document.service';

@Resolver()
export default class DocumentResolver {
  private documentService = new DocumentService;


  @Query(() => [Document], { nullable: true })
  documentSearch(
    @Arg('query', () => Int) q: number
  ): Promise<Document[] | null> {
    return this.documentService.search(q);
  }

  @Query(() => Document, { nullable: true })
  documentView(
    @Arg('query', () => Int) q: number
  ): Promise<Document | null> {
    return this.documentService.view(q);
  }

}
