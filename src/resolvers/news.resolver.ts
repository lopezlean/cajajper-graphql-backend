import { Resolver, Query, Arg, Args, Ctx, Int, FieldResolver, Root } from 'type-graphql';
import ContextInterface from '~/context/context';

import News from '~/entities/news/news.entity';
import NewsService from '~/services/news.service';


const NEWS_VIEW_URL_BASE='https://www.cajajper.gov.ar/secciones/novedades/detalle.php?idNovedad=';
const NEWS_IMAGE_URL_BASE='https://www.cajajper.gov.ar/ss/media/l/';

@Resolver(() => News)
export default class NewsResolver {
  private newsService = new NewsService;

  @FieldResolver(() => String)
  async url(@Root() item: Promise<News>): Promise<string> {
    const i = await item;
    return `${NEWS_VIEW_URL_BASE}${i.id}`;
  }


  @FieldResolver(() => String)
  async image(@Root() item: Promise<News>): Promise<string> {
    const i = await item;
    return `${NEWS_IMAGE_URL_BASE}${i.image}`;
  }


  @Query(() => [News], { nullable: true })
  lastNews(): Promise<News[] | null> {
    return this.newsService.last();
  }

}
