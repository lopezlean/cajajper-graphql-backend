import { Resolver, Query, Arg, Authorized, Int, Ctx } from 'type-graphql';

import ContextInterface from '~/context/context';
import PaycheckList from '~/entities/paycheck/paycheck_list.entity';
import Paycheck from '~/entities/paycheck/paycheck.entity';
import PaycheckService from '~/services/paycheck.service';

@Resolver()
export default class PaycheckResolver {
  private paycheckService: PaycheckService = new PaycheckService();

  @Authorized()
  @Query(() => [PaycheckList])
  async paycheckList(
    @Arg('account_id', () => Int) account_id: number,
    @Ctx() { loggedInUser }: ContextInterface
  ): Promise<PaycheckList[]> {
    return this.paycheckService.list(loggedInUser.id, account_id);

  }

  @Authorized()
  @Query(() => [Paycheck])
  paycheckView(
    @Arg('account_id', () => Int) account_id: number,
    @Arg('year', () => Int) year: number,
    @Arg('month', () => Int) month: number,
    @Arg('annex_id', () => Int) annex_id: number,
    @Arg('file_type_id', () => Int) file_type_id: number,
    @Arg('type', () => String) type: string,

    @Ctx() { loggedInUser }: ContextInterface
  ): Promise<Paycheck[]> {
    return this.paycheckService.view(loggedInUser.id, account_id, year, month, type, annex_id, file_type_id);
  }

  @Authorized()
  @Query(() => String)
  paycheckPdfView(
    @Arg('account_id', () => Int) account_id: number,
    @Arg('year', () => Int) year: number,
    @Arg('month', () => Int) month: number,
    @Arg('annex_id', () => Int) annex_id: number,
    @Arg('file_type_id', () => Int) file_type_id: number,
    @Arg('type', () => String) type: string,

    @Ctx() { loggedInUser }: ContextInterface
  ): Promise<string> {
    return this.paycheckService.viewPDF(loggedInUser.id, account_id, year, month, type, annex_id, file_type_id);
  }
}
