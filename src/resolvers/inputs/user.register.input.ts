import { InputType, Field } from 'type-graphql';

import User  from '~/entities/user.entity';


@InputType({ description: 'New recipe data' })
export default class UserRegisterInput implements Partial<User> {
  @Field()
  email: string;

  @Field({ nullable: true })
  password: string;

  @Field({ nullable: true })
  first_name?: string;

  @Field({ nullable: true })
  last_name?: string;


  @Field({ nullable: true })
  active: boolean;
}
