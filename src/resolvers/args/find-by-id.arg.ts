import { ArgsType, Field } from 'type-graphql';

@ArgsType()
export class FindByIdArg {
  @Field(type => String)
  id: String;
}
