import { ArgsType, Field } from 'type-graphql';

@ArgsType()
export class FindByUserArg {
  @Field(type => String)
  user_id: String;
}
