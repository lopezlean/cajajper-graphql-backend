import { ArgsType, Field } from 'type-graphql';

@ArgsType()
export default class LoginArg {
  @Field()
  email: string;

  @Field()
  password: string;
}
