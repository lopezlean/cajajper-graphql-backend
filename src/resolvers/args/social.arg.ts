import { ArgsType, Field } from 'type-graphql';

@ArgsType()
export class SocialArg {
  @Field(type => String)
  token: String;

}
