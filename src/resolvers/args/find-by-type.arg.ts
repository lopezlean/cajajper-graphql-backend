import { ArgsType, Field } from 'type-graphql';

@ArgsType()
export class FindByTypeArg {
  @Field(type => String)
  type: string;
}
