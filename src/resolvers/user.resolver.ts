import { Resolver, Query, Mutation, Arg, Args, Authorized, Int, Ctx, Root, FieldResolver } from 'type-graphql';

import LoginArg from './args/login.arg';
import RegisterInput from './inputs/user.register.input';
import ContextInterface from '~/context/context';
import Auth from '~/entities/auth.entity';
import User from '~/entities/user.entity';
import UserService from '~/services/user.service';
import Document from '~/entities/documents/document.entity';
import DocumentService from '~/services/document.service';

@Resolver(User)
export default class UserResolver {
  private userService: UserService = new UserService();
  private documentService: DocumentService = new DocumentService();


  @Authorized()
  @Query(() => User)
  myInfo(
    @Ctx() { loggedInUser }: ContextInterface
  ): Promise<User> {
    return this.userService.get(loggedInUser.id);
  }

  @Mutation(() => Auth)
  login(@Args() { email, password }: LoginArg): Promise<Auth> {
    return this.userService.login(email, password);
  }


  @Authorized()
  @Mutation(() => Boolean)
  addToDocumentFavorites(
    @Arg('document_id', () => Int) document_id: number,
    @Ctx() { loggedInUser }: ContextInterface
  ): Promise<boolean> {
    return this.userService.addToFavorites(loggedInUser.id, document_id);
  }

  @Authorized()
  @Mutation(() => Boolean)
  removeFromDocumentFavorites(
    @Arg('document_id', () => Int) document_id: number,
    @Ctx() { loggedInUser }: ContextInterface
  ): Promise<boolean> {
    return this.userService.removeFromFavorites(loggedInUser.id, document_id);
  }

  @Authorized()
  @Mutation(() => Boolean)
  paycheckAssociate(
    @Arg('email') email: string,
    @Arg('password') password: string,
    @Ctx() { loggedInUser }: ContextInterface
  ): Promise<boolean> {
    return this.userService.paycheckAssociate(email, password, loggedInUser.id);
  }

  @Mutation(() => Auth)
  paycheckLogin(@Args() { email, password }: LoginArg): Promise<Auth> {
    return this.userService.paycheckLogin(email, password);
  }
  /**
   * Register
   * @param userData
   */
  @Mutation(() => User)
  register(@Arg('data') data: RegisterInput): Promise<User> {
    return this.userService.register(data as User);
  }

  /**
   * Register
   * @param userData
   */
  @Mutation(() => User)
  resendActivationCode(@Arg('email') email: string): Promise<User> {
    return this.userService.resendActivationCode(email);
  }

  /**
   * Register
   * @param userData
   */
  @Mutation(() => Auth)
  validateUser(
    @Arg('email') email: string,
    @Arg('activationCode', () => Int) activationCode: number
  ): Promise<Auth> {
    return this.userService.activateUser(email, activationCode);
  }

  @Authorized()
  @Query(() => String)
  helloUser(): string {
    return 'hello user';
  }
}
