import { RESTDataSource } from 'apollo-datasource-rest';

import Document from '~/entities/documents/document.entity';

const endpoint = 'http://api.cajajper.gov.ar';
/**
 * @deprecated This class was replaced with in-project graphql code
 */
export default class RestApiCajajper extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = endpoint;
  }

  async novedades(options = {}) {
    const data = await this.get('novedades', options);
    return data.novedades;
  }
  buscarExpedientes = async (options = {}): Promise<[Document?]> => {
    const data = await this.get('expedientes/buscar', options);

    return data.expedientes;
  };
  async verExpediente(id): Promise<Document> {
    const data = await this.get(`expedientes/ver/${id}`);
    console.log(data);
    return data.expediente;
  }
}
