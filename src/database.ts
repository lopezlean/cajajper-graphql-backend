import { createConnections, Connection } from 'typeorm';

import Config from './config';

class Database {
  // Init database
  //protected initDatase: InitDatabase;
  public connections: Connection[];

  async connect(): Promise<void> {
    const connect = async () => {
      this.connections = await createConnections([
        {
          entities: [`${__dirname}/entities/*.ts`],
          name: 'default',
          subscribers: [`${__dirname}/subscribers/**/*.ts`],
          type: 'mongodb',
          url: Config.MONGODB_URI,
          useUnifiedTopology: true
        },
        {
          entities: [`${__dirname}/entities/paycheck/**/*.ts`],
          name: 'recibo',
          type: 'mysql',
          url: Config.RECIBO_DIGITAL_URI
        },
        {
          entities: [`${__dirname}/entities/documents/**/*.ts`],
          name: 'expedientes',
          type: 'mysql',
          url: Config.EXPEDIENTES_URI
        },
        {
          entities: [`${__dirname}/entities/news/**/*.ts`],
          name: 'novedades',
          type: 'mysql',
          url: Config.NOVEDADES_URI
        }
      ]);
    };
    await connect();
    //  mongoose.connection.on('disconnected', connect);
  }

  close(): void {
    //    mongoose.disconnect(done);
  }
}

export default Database;
