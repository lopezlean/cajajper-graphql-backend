import BaseException from './basic.exception';

export default class UnauthorizedException extends BaseException {
    constructor (public message = 'unauthorized exception', cause = []) {
        super();
        this.status = 401;
        this.message = message;
        this.cause = cause;
    }
}
