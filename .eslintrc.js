const ERROR = 'error';
const OFF = 'off';

const BASIC_RULES = {
  'no-await-in-loop': ERROR,
  'no-dupe-else-if': ERROR
};
const BEST_PRACTICES_RULES = {
  curly: [ERROR, 'all'],
  'dot-notation': ERROR,
  eqeqeq: ERROR,
  'no-else-return': ERROR,
  'no-script-url': ERROR,
  'no-useless-concat': ERROR
};
const VARIABLE_RULES = {
  'no-use-before-define': [ERROR, { classes: true, functions: true, variables: true }]
};
const STYLISTIC_RULES = {
  'sort-keys': ERROR
};
const ECMASCRIPT6_RULES = {
  'no-duplicate-imports': [ERROR, { includeExports: true }],
  'no-useless-computed-key': [ERROR, { enforceForClassMembers: true }],
  'no-useless-constructor': ERROR,
  'no-var': ERROR,
  'object-shorthand': [ERROR, 'always', {}],
  'prefer-const': ERROR,
  'prefer-destructuring': OFF,
  'prefer-rest-params': ERROR,
  'prefer-spread': ERROR,
  'prefer-template': ERROR
};

const TYPESCRIPT_TYPES = {
  '@typescript-eslint/explicit-function-return-type': OFF,
  '@typescript-eslint/explicit-member-accessibility': OFF,
  '@typescript-eslint/member-delimiter-style': OFF,
  '@typescript-eslint/member-ordering': 'error',
  '@typescript-eslint/no-angle-bracket-type-assertion': OFF,
  '@typescript-eslint/no-explicit-any': OFF,
  '@typescript-eslint/no-inferrable-types': OFF,
  '@typescript-eslint/no-parameter-properties': OFF,
  '@typescript-eslint/no-unused-vars': [ERROR, 'none'],
  'type-graphql/wrong-decorator-signature': OFF
};

/* eslint-disable sort-keys */
const TYPESCRIPT_RULES = {
  'no-use-before-define': OFF,
  '@typescript-eslint/no-use-before-define': [ERROR],
  '@typescript-eslint/no-unused-vars': [ERROR]
};
/* eslint-enable sort-keys */

module.exports = {
  parser: '@typescript-eslint/parser', // Specifies the ESLint parser
  parserOptions: {
    project: './tsconfig.eslint.json',

    ecmaVersion: 2020, // Allows for the parsing of modern ECMAScript features
    sourceType: 'module' // Allows for the use of imports
  },
  extends: [
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking',
    'plugin:type-graphql/recommended',
    'prettier'
  ],

  plugins: ['typeorm', 'type-graphql', 'import', 'eslint-plugin-import-helpers', 'optimize-regex'],
  rules: {
    // ESLINT
    ...BASIC_RULES,
    ...BEST_PRACTICES_RULES,
    ...VARIABLE_RULES,
    ...STYLISTIC_RULES,
    ...ECMASCRIPT6_RULES,
    ...TYPESCRIPT_TYPES,
    // PLUGIN-IMPORT-HELPERS
    'import-helpers/order-imports': [
      'warn',
      {
        alphabetize: { ignoreCase: true, order: 'asc' },
        groups: ['module', '/^cajajper/', ['absolute', '/^~/', 'parent', 'sibling', 'index']],
        newlinesBetween: 'always'
      }
    ],
    // IMPORT OVERRIDES
    'import/no-duplicates': ERROR,
    'import/no-unresolved': OFF,
    // OPTIMIZE REGEX
    'optimize-regex/optimize-regex': ERROR,
    ...TYPESCRIPT_RULES
  }
};
