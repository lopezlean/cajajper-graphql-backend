module.exports = {
  '*.md': ['prettier --write'],
  '*.{js,ts}': ['npm run fix:lint:js']
};
